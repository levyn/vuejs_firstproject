<?php
session_start();
header("content-type: application/json");
include("../libs/functions.php");

init_todos();

if(isset($_POST["texte"]) && $_POST["texte"] != ""){
  $todo = array("id" => uniqid(), "texte" => $_POST["texte"], "date" => time(), "termine" => false);
  // Sauvegarder dans la Session.
  $_SESSION["todos"][$todo["id"]] = $todo;
  // Afficher un JSON
  Jreturn(array("success" => true));
}else{
  Jreturn(array("success" => false));
}

?>