<?php
session_start();
header("content-type: application/json");
include("../libs/functions.php");

init_todos();

if(isset($_GET["id"]) && $_GET["id"] != ""){
  if(isset($_SESSION["todos"][$_GET["id"]]) && $_SESSION["todos"][$_GET["id"]]["termine"] == true){
  	unset($_SESSION["todos"][$_GET["id"]]);
  	Jreturn(array("success" => true));
  }
  else
  	Jreturn(array("success" => false));

}else{
  Jreturn(array("success" => false));
}

?>